from elasticsearch import Elasticsearch
from datetime import datetime

es = Elasticsearch(hosts='http://localhost:9200')


def insert_demo_data():
    #  insert into index
    response = es.index(
       index = 'monitoring',
       document = {
           'hostname': 'test.fw.local',
           'status': 'OK',
           'timestamp': datetime.now(),
       })
    print(response['result'])


def get_single_result():
    # get from index
    doc = {
       'hostname': 'test.fw.local',
    }
    response = es.index(index='monitoring', document=doc)
    print(response['result'])


# get count
def get_count():
    resp = es.search(index="monitoring", query={"match_all": {}})
    return resp['hits']['total']['value']


# get all results
def get_result():
    resp = es.search(index="monitoring", query={"match_all": {}})
    print("Got %d Hits:" % resp['hits']['total']['value'])

    for hit in resp['hits']['hits']:
        if 'status' in hit["_source"]:
            print("%(hostname)s: %(status)s" % hit["_source"])
