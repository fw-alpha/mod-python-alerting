from setuptools import setup

setup(
    name='mod-python-alerting',
    version='1.0',
    description='add e-mail alerting to elasticsearch for free',
    author='Anton Papp',
    author_email='anton.papp@formware.de',
    packages=['lib'],  # same as name
    install_requires=['elasticsearch>=7.0.0,<8.0.0'],  # external packages as dependencies
)



